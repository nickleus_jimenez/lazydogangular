/**
 * 
 */
function GetResponseFromserverAsync(containerSelector, givenUrl) {
    if ($(containerSelector).length > 0) {
        $.ajax({
            type: "GET",
            url: givenUrl,
            contentType: false,
            processData: false,
            success: function (info) {
                $(containerSelector).html(info);
            },
            error: function (xhr, status, p3, p4) {
                $(".uploadStatus").text("Upload Failed ");
            }
        });
    }
}
//var data = JSON.parse(json);
function GetJsonExcusesFromAjax(containerSelector, givenUrl) {
    if ($(containerSelector).length > 0) {
        $.ajax({
            type: "GET",
            url: givenUrl,
            contentType: false,
            processData: false,
            success: function (info) {
            	var data = JSON.parse(info);
            	$(containerSelector).html("");
            	for(var ctr=0;ctr<data.excuses.length;ctr++){
            		$(containerSelector).append("<p>"+data.excuses[ctr]+"</p>");
            	}
            	
            },
            error: function (xhr, status, p3, p4) {
                $(".uploadStatus").text("Upload Failed ");
            }
        });
    }
}