<html>
<head>
<title>LazyDog</title>
<!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">

  <!-- Compiled and minified JavaScript -->
  
<%@ include file="partials/ScriptCalls.jsp" %>
</head>
<body>
<nav class="light-violet lighten-1" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">Logo</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="Kanban.jsp">Kanban</a></li>
      </ul>

      <ul id="nav-mobile" class="side-nav" style="transform: translateX(-100%);">
        <li><a href="#">Navbar Link</a></li>
      </ul>
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
  </nav>
  <div class="row" >
    Name: <input/>
    <h1>You entered: </h1>
    <h1>Hello World To Lazy Dog</h1>
</div>
<div class="row cyan">
	<a class="btn orange" onclick="GetJsonExcusesFromAjax('.Stage', '<%= excusesUrl %>')">Make Excuse</a>
</div>
<div class="Stage"></div>
</body>
</html>
