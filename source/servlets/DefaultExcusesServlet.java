package servlets;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;
/**
 * Servlet implementation class DefaultExcusesServlet
 */
@WebServlet(description = "Loads a story", urlPatterns = { "/Excuses" })
public class DefaultExcusesServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public DefaultExcusesServlet() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void executeCustomCode(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		PrintWriter out=null;
		try{
			out = response.getWriter();
			out.print("{\"excuses\":[\"LBM\",\"Migraine\",\"Sore Eyes\",\"Twisted Ankle\"],\"valid\":\"true\"}");
		}
		catch(Exception ex){
			out.println("Error in getting the story\n"+ex.getMessage());
		}
		out.flush();
	}

}
